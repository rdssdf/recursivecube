#----------------------------------------------------------
# File objects.py
#----------------------------------------------------------
import bpy
import mathutils
import math
from mathutils import Vector

# print all objects
#for obj in bpy.data.objects:
 #   print(obj.name)


# print all scene names in a list
#print(bpy.data.scenes.keys())

def createInitialCube (name, origin, x = 1):
	bpy.ops.mesh.primitive_cube_add(radius=1.0)
	
	cube = bpy.context.object
	cube.name = name
	cube.show_name = True
	cube.scale=((x, 1, 1))
	
	mesh = cube.data
	mesh.name = name+'Mesh'
	
	return cube

def stretchCube(cube, xfactor, yfactor, zfactor):
	cube.scale=((xfactor, yfactor, zfactor))
	return

def applyDifference(target, opObj):
	'''subtract opObj from the target'''

	selectOnly(target)

	modifierName = 'boolean_difference'
	bool_one = target.modifiers.new(type="BOOLEAN", name=modifierName)
	bool_one.operation = 'DIFFERENCE'
	bool_one.object = opObj

	bpy.ops.object.modifier_apply(modifier=modifierName)

	return

def hollowOutCube(cube, barThickness = 0.5):
	hollowMass = createHollowMass(cube, barThickness)
	
	applyDifference(cube, hollowMass)
	
	selectOnly(hollowMass)
	bpy.ops.transform.rotate(axis=((1,0,0)))
	
	applyDifference(cube, hollowMass)
	
	deleteOnly(hollowMass)
	
	return

def createHollowMass(cube, scalefactor=0.5):
	
	copy = copyToScene(cube, 'hollowMass')
	
	scaledX = cube.scale.x+1
	scaledY = cube.scale.y*scalefactor
	scaledZ = cube.scale.z+1
	
	copy.scale = ((scaledX, scaledY, scaledZ))
		
	return copy

def movePivotToCorner(edge):
	scale = edge.scale
	x = scale.x
	y = scale.y
	z = -scale.z
	bpy.context.scene.cursor_location = ((x,y,z))
	
	return

def moveOriginToPivot(obj):
	selectOnly(obj)
	bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
	return

def moveEdgeCornerToPivot(edge):
	saved_location = Vector(bpy.context.scene.cursor_location)
	movePivotToCorner(edge)
	moveOriginToPivot(edge)
	bpy.context.scene.cursor_location = saved_location
	print('saved location: '+str(saved_location))
	edge.location = saved_location
		
	return

def createCorner(edge):
	moveEdgeCornerToPivot(edge)
	
	bpy.ops.transform.rotate(value=math.pi, axis=((0,0,1)))
	
	verticalEdge = copyToScene(edge, 'verticalEdge')

	selectOnly(verticalEdge)

	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((0,1,0)))
	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((0,0,1)))

	horizontalEdge = copyToScene(verticalEdge, 'horizontalEdge')

	selectOnly(horizontalEdge)
	
	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((1,0,0)))
	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((0,1,0)))
	
	edge.select = True	
	horizontalEdge.select = True
	verticalEdge.select = True
	
	bpy.ops.object.join()
	
	corner = bpy.context.object
	corner.name = 'Corner'
	
	return corner

def createEdge(corner):
	other = copyToScene(corner, 'corner1')
	selectOnly(other)

	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((1,0,0)))
	other.location=((0,0,other.scale.x*4))
	
	corner.select = True

	bpy.ops.object.join()

	edge = bpy.context.object

	selectOnly(edge)
	moveOriginToPivot(edge)
	edge.name = 'Edge'
	
	return edge

def movePivotToInnerCenter(side):
	scale = side.scale
	x = 2*scale.x
	#y = x
	#z = x
	
	bpy.context.scene.cursor_location = ((x,x,x))
	
	return

def createSide(edge):

	saved_origin = Vector(bpy.context.scene.cursor_location)

	other = copyToScene(edge, 'edge1')
	selectOnly(other)
	
	bpy.ops.transform.rotate(value=math.pi*1.5, axis=((0,0,1)))
	other.location=((0, other.scale.x*4, 0))
	
	edge.select = True
	bpy.ops.object.join()
	
	side = bpy.context.object

	movePivotToInnerCenter(side)
	bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
	
	bpy.context.scene.cursor_location = saved_origin
	side.location = saved_origin
	
	return side

def createCube(side):
	other = copyToScene(side, 'side1')
	selectOnly(other)
	
	bpy.ops.transform.rotate(value=math.pi, axis=((0,0,1)))

	side.select = True
	bpy.ops.object.join()
	
	return other

def run(origo):
	origin = Vector(origo)
	bpy.context.scene.cursor_location = origo

	x = 30
	cube = createInitialCube('shape', origin, x)
	
	barThickness = 0.7
	hollowOutCube(cube, barThickness)
		
	print('edge location: '+str(cube.location))
	
	cube = createCorner(cube)

	cube = createEdge(cube)

	cube = createSide(cube)
	
	cube = createCube(cube)
	
	return

def copyToScene(cube, name):
	# duplicate linked
	copy = cube.copy()
	# optional: make this a real duplicate (not linked)
	copy.data = cube.data.copy()
	copy.name = name
	bpy.context.scene.objects.link(copy) # add to scene
	
	return copy

#def selectOnlyByName(name):
#	obj = bpy.data.objects[name];
#	return selectOnly(obj)

def selectOnly(obj):
	bpy.ops.object.select_all(action='DESELECT')
	obj.select = True
	bpy.context.scene.objects.active = obj

	return obj

def deleteOnly(obj):
	selectOnly(obj)
	bpy.ops.object.delete() 
	return

#def deleteObjectByName(name):
#	selectOnlyByName(name)
#	bpy.ops.object.delete() 
#	return

if __name__ == "__main__":
	run((0,0,0))
	


#(x,y,z) = (0.707107, 0.258819, 0.965926)
#verts = ((x,x,-1), (x,-x,-1), (-x,-x,-1), (-x,x,-1), (0,0,1))
#faces = ((1,0,4), (4,2,1), (4,3,2), (4,0,3), (0,1,2,3))
	
#cone1 = createMeshFromData('DataCone', origin, verts, faces)
#cone2 = createMeshFromOperator('OpsCone', origin+Vector((0,2,0)), verts, faces)
#cone3 = createMeshFromPrimitive('PrimCone', origin+Vector((0,4,0)))
 
#rig1 = createArmatureFromData('DataRig', origin+Vector((0,6,0)))
#rig2 = createArmatureFromOperator('OpsRig', origin+Vector((0,8,0)))
#rig3 = createArmatureFromPrimitive('PrimRig', origin+Vector((0,10,0)))
	
#   cube = createMeshFromOperator('OpsCube', origin+Vector((0,2,0)), verts, faces)


#################################


### some other ideas from wiki

def createMeshFromData(name, origin, verts, faces):
	# Create mesh and object
	me = bpy.data.meshes.new(name+'Mesh')
	ob = bpy.data.objects.new(name, me)
	ob.location = origin
	ob.show_name = True
 
	# Link object to scene and make active
	scn = bpy.context.scene
	scn.objects.link(ob)
	scn.objects.active = ob
	ob.select = True
 
	# Create mesh from given verts, faces.
	me.from_pydata(verts, [], faces)
	# Update mesh with new data
	me.update() 
	return ob
 
def createMeshFromOperator(name, origin, verts, faces):
	bpy.ops.object.add(
		type='MESH', 
		enter_editmode=False,
		location=origin)
	ob = bpy.context.object
	ob.name = name
	ob.show_name = True
	me = ob.data
	me.name = name+'Mesh'
 
	# Create mesh from given verts, faces.
	me.from_pydata(verts, [], faces)
	# Update mesh with new data
	me.update() 
	# Set object mode
	bpy.ops.object.mode_set(mode='OBJECT')
	return ob
 
def createMeshFromPrimitive(name, origin):
	bpy.ops.mesh.primitive_cone_add(
		vertices=4, 
		radius=1, 
		depth=1, 
		cap_end=True, 
		view_align=False, 
		enter_editmode=False, 
		location=origin, 
		rotation=(0, 0, 0))
 
	ob = bpy.context.object
	ob.name = name
	ob.show_name = True
	me = ob.data
	me.name = name+'Mesh'
	return ob
 
def createArmatureFromData(name, origin):
	# Create armature and object
	amt = bpy.data.armatures.new(name+'Amt')
	ob = bpy.data.objects.new(name, amt)
	ob.location = origin
	ob.show_name = True
 
	# Link object to scene and make active
	scn = bpy.context.scene
	scn.objects.link(ob)
	scn.objects.active = ob
	ob.select = True
 
	# Create single bone
	bpy.ops.object.mode_set(mode='EDIT')
	bone = amt.edit_bones.new('Bone')
	bone.head = (0,0,0)
	bone.tail = (0,0,1)
	bpy.ops.object.mode_set(mode='OBJECT')
	return ob
 
def createArmatureFromOperator(name, origin):
	bpy.ops.object.add(
		type='ARMATURE', 
		enter_editmode=True,
		location=origin)
	ob = bpy.context.object
	ob.name = name
	ob.show_name = True
	amt = ob.data
	amt.name = name+'Amt'
 
	# Create single bone
	bone = amt.edit_bones.new('Bone')
	bone.head = (0,0,0)
	bone.tail = (0,0,1)
	bpy.ops.object.mode_set(mode='OBJECT')
	return ob
 
def createArmatureFromPrimitive(name, origin):
	bpy.ops.object.armature_add()
	bpy.ops.transform.translate(value=origin)
	ob = bpy.context.object
	ob.name = name
	ob.show_name = True
	amt = ob.data
	amt.name = name+'Amt'
	return ob
 